#!/bin/bash

docker-compose -f docker-compose.yaml -f _1_1_generate_pow_genesis.yaml --project-name docmans_geth up
docker-compose -f docker-compose.yaml -f _1_2_run_pow_network.yaml --project-name docmans_geth up -d
