FROM golang:1.12
ENV GO111MODULE=on
WORKDIR /docmans
# Separate downloading dependencies from building them with the project to
# improve container rebuild times.
COPY ./go.mod ./
COPY ./go.sum ./
RUN go mod download
# Build the actual project
COPY ./ ./
RUN go build -o docmans-backend main.go \
    && cp ./docmans-backend /usr/bin
ENTRYPOINT [ "docmans-backend" ]
