package app

import (
	u "docmans/utils"
	"net/http"
)

var NotFoundHandler = func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		u.Respond(w, u.Message("error", "This resources was not found on our server"))
		next.ServeHTTP(w, r)
	})
}
