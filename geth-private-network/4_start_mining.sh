#!/bin/bash

curl -X POST -H "Content-Type: application/json" --data "{\"method\":\"personal_unlockAccount\",\"params\":[\"$1\", \"$2\", 0],\"id\":1,\"jsonrpc\":\"2.0\"}'" localhost:8545
curl -X POST -H "Content-Type: application/json" --data "{\"method\":\"miner_setEtherbase\",\"params\":[\"$1\"],\"id\":1,\"jsonrpc\":\"2.0\"}" localhost:8545
curl -X POST -H "Content-Type: application/json" --data '{"method":"miner_start","params":[],"id":1,"jsonrpc":"2.0"}' localhost:8545
