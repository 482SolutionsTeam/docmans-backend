package controllers

import (
	"docmans/contract"
	"docmans/models"
	"docmans/service"
	u "docmans/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func CreateDirectory(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)

	directory := &models.Directory{}
	if err := json.NewDecoder(r.Body).Decode(directory); err != nil {
		u.Respond(w, u.Message("error", "Invalid request"))
		return
	}

	fullPathParts := strings.Split(directory.Directory, "/")
	if len(fullPathParts) > 0 {
		if fullPathParts[0] == "" {
			fullPathParts = fullPathParts[1:]
		}
	}
	if len(fullPathParts) > 0 {
		if fullPathParts[len(fullPathParts)-1] == "" {
			fullPathParts = fullPathParts[:len(fullPathParts)-1]
		}
	}

	isShared := false

	if len(fullPathParts) > 0 {
		if fullPathParts[0] == "shared" {
			if len(fullPathParts) < 2 {
				w.WriteHeader(401)
				u.Respond(w, u.Message("error", "cannot get empty path"))
				return
			}
			userId = fullPathParts[1]
			fullPathParts = fullPathParts[2:]
			directory.Directory = strings.Join(fullPathParts, "/")
			isShared = true
		}
	}

	user, err := models.GetUserByID(userId)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user %s", err)))
		return
	}

	dir, errDir := service.CreateDir(userId, directory.Directory)
	if errDir != nil {
		w.WriteHeader(404)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", errDir)))
		return
	}
	if dir != "" {
		w.WriteHeader(409)
		u.Respond(w, u.Message("error", "Directory already exist"))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	cPath, cName := filepath.Split(directory.Directory)

	if isShared {
		// check permssions
		permissions, err := connection.GetPermissions(user.EthereumPrivateKey, cPath)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to get permissions: %s", err)))
			return
		}

		currentUserId := r.Context().Value("user").(string)
		currentUser, err := models.GetUserByID(currentUserId)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user: %s", err)))
			return
		}

		_, address, err := connection.GetKeyAndAddress(currentUser.EthereumPrivateKey)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to get address for the current user: %s", err)))
			return
		}

		isAllowedUser := false
		for _, allowedAddress := range permissions.Write {
			isAllowedUser = isAllowedUser || (*address == allowedAddress)
		}

		if !isAllowedUser {
			w.WriteHeader(403)
			u.Respond(w, u.Message("error", "user not allowed"))
			return
		}
	}

	if err := connection.RegisterFilesystemNode(user.EthereumPrivateKey, cPath, cName, false); err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to register dir: %s", err)))
		return
	}

	u.Respond(w, u.Message("message", "create directory ok"))
}

type UploadFile struct{}

func (uf UploadFile) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)
	fileToSave := &models.File{}
	targetDir := r.URL.Path

	user, err := models.GetUserByID(userId)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user %s", err)))
		return
	}

	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	fileToSave.Name = handler.Filename
	fileToSave.Path = filepath.Join(userId, targetDir)
	fileToSave.Data = fileBytes

	if err := service.AddNewFile(userId, fileToSave); err != nil {
		w.WriteHeader(404)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	if err := connection.RegisterFilesystemNode(user.EthereumPrivateKey, targetDir, handler.Filename, true); err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to register dir: %s", err)))
		return
	}

	u.Respond(w, u.Message("message", "load file ok"))
}

func GetTreeStructure(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)
	resp, err := service.Tree(userId)
	if err != nil {
		w.WriteHeader(400)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
		return
	}
	u.Respond(w, []byte(resp))
}

type DownloadFile struct{}

func (df DownloadFile) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)
	targetFile := r.URL.Path

	if targetFile == "" {
		http.Error(w, "Get 'file' not specified in url.", 400)
		return
	}
	file, err := service.DownloadFile(userId, targetFile)
	if err != nil {
		w.WriteHeader(400)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
		return
	}
	_, filename := filepath.Split(targetFile)
	w.Header().Set("Content-Disposition", "attachment; filename="+filename)
	w.Header().Set("Content-Type", mime.TypeByExtension(filepath.Ext(targetFile)))
	if _, err := w.Write(file.Data); err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
	}
}
