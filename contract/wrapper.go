package contract

import (
	"context"
	"crypto/ecdsa"
	"docmans/service"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"log"
	"math/big"
	"strings"
	"sync"
	"time"
)

const (
	ReadPermission = iota
	WritePermission
)

type Connection struct {
	client           *ethclient.Client
	contractAddress  common.Address
	contractInstance *FileToken
}

func MakeConnection(nodeAddress, contractAddress string) (*Connection, error) {
	connection := &Connection{}
	connection.contractAddress = common.HexToAddress(contractAddress)

	client, err := ethclient.Dial(nodeAddress)
	if err != nil {
		return nil, errors.Wrap(err, "cannot connect to Ethereum node")
	}
	connection.client = client

	instance, err := NewFileToken(connection.contractAddress, connection.client)
	if err != nil {
		return nil, errors.Wrap(err, "failed to instantiate contract")
	}
	connection.contractInstance = instance

	return connection, nil
}

func (c *Connection) Close() {
	c.client.Close()
}

func (c *Connection) GetKeyAndAddress(privateKeyString string) (*ecdsa.PrivateKey, *common.Address, error) {
	// remove 0x
	privateKeyString = privateKeyString[2:]
	privateKey, err := crypto.HexToECDSA(privateKeyString)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to parse private key")
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, nil, errors.New("failed to get public key")
	}
	ethAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	return privateKey, &ethAddress, nil
}

func (c *Connection) getTransactionOpts(privateKeyString string) (*bind.TransactOpts, error) {
	privateKey, ethAddress, err := c.GetKeyAndAddress(privateKeyString)
	if err != nil {
		return nil, err
	}

	auth := bind.NewKeyedTransactor(privateKey)
	auth.Value = big.NewInt(0)
	auth.GasLimit = uint64(5000000)
	auth.GasPrice = big.NewInt(0)

	nonce, err := c.client.PendingNonceAt(context.Background(), *ethAddress)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get nonce")
	}

	auth.Nonce = big.NewInt(int64(nonce))

	return auth, nil
}

func (c *Connection) RegisterRootDir(privateKey, username string) error {
	auth, err := c.getTransactionOpts(privateKey)
	if err != nil {
		return errors.Wrap(err, "failed to build a transaction signer")
	}

	if _, err := c.contractInstance.RegisterRootDirectory(auth, username); err != nil {
		return errors.Wrap(err, "while sending a registerRootDir transaction")
	}

	return nil
}

func (c *Connection) RegisterFilesystemNode(privateKey, path, nodeName string, isFile bool) error {
	auth, err := c.getTransactionOpts(privateKey)
	if err != nil {
		return errors.Wrap(err, "failed to build a transaction signer")
	}

	pathList := strings.Split(path, "/")
	if len(pathList) > 0 {
		if pathList[0] == "" {
			pathList = pathList[1:]
		}
	}
	if len(pathList) > 0 {
		if pathList[len(pathList)-1] == "" {
			pathList = pathList[:len(pathList)-1]
		}
	}

	if _, err := c.contractInstance.AddChild(auth, nodeName, pathList, isFile); err != nil {
		return errors.Wrap(err, "while sending an addChild transaction")
	}

	if len(pathList) == 0 {
		return nil
	}

	time.Sleep(time.Second)

	permissions, err := c.GetPermissions(privateKey, path)
	if err != nil {
		return errors.Wrap(err, "failed to get permissions for the parent dir")
	}

	for _, permittedAddr := range permissions.Read {
		auth, err := c.getTransactionOpts(privateKey)
		if err != nil {
			return errors.Wrap(err, "failed to build a transaction signer")
		}
		if _, err := c.contractInstance.AddPermission(auth, append(pathList, nodeName), permittedAddr, ReadPermission); err != nil {
			return errors.Wrap(err, "while sending an addChild transaction")
		}
	}
	for _, permittedAddr := range permissions.Write {
		auth, err := c.getTransactionOpts(privateKey)
		if err != nil {
			return errors.Wrap(err, "failed to build a transaction signer")
		}
		if _, err := c.contractInstance.AddPermission(auth, append(pathList, nodeName), permittedAddr, WritePermission); err != nil {
			return errors.Wrap(err, "while sending an addChild transaction")
		}
	}

	return nil
}

type Permissions struct {
	Read  []common.Address
	Write []common.Address
	Owner common.Address
}

func (c *Connection) GetPermissions(privateKeyString, path string) (*Permissions, error) {
	_, address, err := c.GetKeyAndAddress(privateKeyString)
	if err != nil {
		return nil, err
	}

	pathList := strings.Split(path, "/")
	if len(pathList) > 0 {
		if pathList[0] == "" {
			pathList = pathList[1:]
		}
	}
	if len(pathList) > 0 {
		if pathList[len(pathList)-1] == "" {
			pathList = pathList[:len(pathList)-1]
		}
	}

	log.Printf("path %v", pathList)
	permissionsRaw, err := c.contractInstance.GetPermissions(nil, *address, pathList)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get permissions")
	}

	log.Printf("got permissions %v", permissionsRaw)

	permissions := &Permissions{
		Read:  []common.Address{},
		Write: []common.Address{},
		Owner: permissionsRaw.Owner,
	}
	for i, userAddress := range permissionsRaw.User {
		if permissionsRaw.PermissionType[i] == 0 {
			permissions.Read = append(permissions.Read, userAddress)
		} else {
			permissions.Write = append(permissions.Write, userAddress)
		}
	}
	return permissions, errors.Wrap(err, "while getting permissions")
}

func (c *Connection) AddPermission(privateKeyString, path string, address common.Address, permission uint8) error {
	auth, err := c.getTransactionOpts(privateKeyString)
	if err != nil {
		return errors.Wrap(err, "failed to build a transaction signer")
	}

	pathList := strings.Split(path, "/")
	if len(pathList) > 0 {
		if pathList[0] == "" {
			pathList = pathList[1:]
		}
	}
	if len(pathList) > 0 {
		if pathList[len(pathList)-1] == "" {
			pathList = pathList[:len(pathList)-1]
		}
	}

	if len(pathList) == 0 {
		return errors.New("cannot perform this operation for the root directory")
	}

	log.Printf("path %v", pathList)
	if _, err := c.contractInstance.AddPermission(auth, pathList, address, permission); err != nil {
		return errors.Wrapf(err, "while adding permissions for %v", pathList)
	}

	return nil
}

func (c *Connection) getSharedDirectoriesHelper(address common.Address, path []string, errOutput chan<- error, resultOutput chan<- service.Element) {
	result, err := c.contractInstance.GetSharedDirectories(nil, address, path)
	if err != nil {
		err = errors.Wrapf(err, "while getting shared directories, path: %v", path)
		log.Printf("error: %s", err)
		errOutput <- err
		return
	}

	log.Printf("dir %v path response: %v", path, result)

	directories := result.Dirs
	files := result.Files

	directory := service.Element{}
	directory.Entries = []interface{}{}
	for _, file := range files {
		directory.Entries = append(directory.Entries, file)
	}
	if len(path) == 0 {
		directory.Directory = "shared"
	} else {
		directory.Directory = path[len(path)-1]
	}

	subRoutineErr := make(chan error, len(directories))
	subRoutineResult := make(chan service.Element, len(directories))

	wg := sync.WaitGroup{}
	wg.Add(len(directories))

	for i, _ := range directories {
		go func(idx int) {
			newPath := append(path, directories[idx])
			c.getSharedDirectoriesHelper(address, newPath, subRoutineErr, subRoutineResult)
			wg.Done()
		}(i)
	}

	wg.Wait()

	select {
	case err := <-subRoutineErr:
		errOutput <- err
		return
	default:
	}

	close(subRoutineResult)

	for entry := range subRoutineResult {
		directory.Entries = append(directory.Entries, entry)
	}

	resultOutput <- directory
}

func (c *Connection) GetSharedDirectories(privateKeyString string) (*service.Element, error) {
	_, address, err := c.GetKeyAndAddress(privateKeyString)
	if err != nil {
		return nil, err
	}

	finished := make(chan bool)

	subRoutineErr := make(chan error, 1)
	subRoutineResult := make(chan service.Element, 1)

	go func() {
		c.getSharedDirectoriesHelper(*address, []string{}, subRoutineErr, subRoutineResult)
		finished <- true
	}()

	<-finished

	select {
	case err := <-subRoutineErr:
		return nil, err
	default:
	}

	result := <-subRoutineResult

	return &result, nil
}

func (c *Connection) AddressToUsername(address common.Address) (string, error) {
	return c.contractInstance.AddressToUsername(nil, address)
}
