package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func Message(messageType string, message string) []byte {
	mapVar1 := map[string]string{messageType: message}
	mapVar2, _ := json.Marshal(mapVar1)
	return mapVar2
}

func Respond(w http.ResponseWriter, data []byte) {
	w.Header().Add("Content-Type", "application/json")
	_, err := w.Write(data)
	if err != nil {
		fmt.Println(err)
	}
}
