package service

import (
	"encoding/json"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

type Element struct {
	Directory string        `json:"directory"`
	Entries   []interface{} `json:"entries"`
}

func Tree(rootPath string) (tree []byte, err error) {
	peerId, err := findPeerId(rootPath)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to find peer id based on usedId: %s", rootPath)
	}

	resolvedCid, err := resolveIpns(peerId)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to resolve ipns from peerId: %s", rootPath)
	}

	if err := getDir(resolvedCid, rootPath); err != nil {
		return nil, errors.Wrapf(err, "Failed to get user's %s dir from ipfs", rootPath)
	}

	defer clean(rootPath)

	root := Element{}
	root.Directory = rootPath
	root.Entries = []interface{}{}
	if err := buildDir(rootPath, &root); err != nil {
		return nil, err
	}

	res, _ := json.Marshal(root)
	return res, nil
}

func buildDir(path string, parent *Element) (err error) {
	f, err := os.Open(path)

	if err != nil {
		return errors.Wrapf(err, "Failed to open `%s` path", path)
	}

	fileInfo, err := f.Readdir(-1)
	if err != nil {
		return errors.Wrap(err, "Failed to read dir")
	}
	if err := f.Close(); err != nil {
		return errors.Wrap(err, "failed to close dir")
	}

	for _, file := range fileInfo {
		if !file.IsDir() {
			parent.Entries = append(parent.Entries, file.Name())
		} else {
			filePath := filepath.Join(path, file.Name())
			subDir := Element{}
			subDir.Directory = file.Name()
			subDir.Entries = []interface{}{}
			if err := buildDir(filePath, &subDir); err != nil {
				return errors.Wrap(err, "Failed to build dir content")
			}
			parent.Entries = append(parent.Entries, subDir)
		}
	}

	return nil
}
