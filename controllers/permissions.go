package controllers

import (
	"docmans/contract"
	"docmans/models"
	"docmans/service"
	u "docmans/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type ReadPermissions struct{}

func (ReadPermissions) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)
	target := r.URL.Path

	fullPathParts := strings.Split(target, "/")
	if len(fullPathParts) > 0 {
		if fullPathParts[0] == "" {
			fullPathParts = fullPathParts[1:]
		}
	}
	if len(fullPathParts) > 0 {
		if fullPathParts[len(fullPathParts)-1] == "" {
			fullPathParts = fullPathParts[:len(fullPathParts)-1]
		}
	}

	resultPretty := struct {
		Owner string   `json:"owner"`
		View  []string `json:"view"`
		Edit  []string `json:"edit"`
	}{
		View: []string{},
		Edit: []string{},
	}

	if len(fullPathParts) > 0 {
		if fullPathParts[0] == "shared" {
			if len(fullPathParts) < 2 {
				w.WriteHeader(401)
				u.Respond(w, u.Message("error", "cannot get empty path"))
				return
			}
			userId = fullPathParts[1]
			fullPathParts = fullPathParts[2:]
			target = strings.Join(fullPathParts, "/")
		}
	}

	if len(fullPathParts) == 0 {
		w.WriteHeader(401)
		u.Respond(w, u.Message("error", "cannot get empty path"))
		return
	}

	user, err := models.GetUserByID(userId)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user %s", err)))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	result, err := connection.GetPermissions(user.EthereumPrivateKey, target)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while getting permissions: %s", err)))
		return
	}

	resultPretty.Owner, err = connection.AddressToUsername(result.Owner)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while getting permissions: %s", err)))
		return
	}

	for _, address := range result.Read {
		name, err := connection.AddressToUsername(address)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("while getting permissions: %s", err)))
			return
		}
		resultPretty.View = append(resultPretty.View, name)
	}

	for _, address := range result.Write {
		name, err := connection.AddressToUsername(address)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("while getting permissions: %s", err)))
			return
		}
		resultPretty.Edit = append(resultPretty.Edit, name)
	}

	resultJSON, err := json.Marshal(resultPretty)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while serializing permissions: %s", err)))
		return
	}
	w.Header().Add("Content-Type", "application/json")
	if _, err := w.Write(resultJSON); err != nil {
		fmt.Println(err)
	}
}

type SetPermissionsRequest struct {
	Read  []string `json:"View"`
	Write []string `json:"Edit"`
}

type SetPermissions struct{}

func (SetPermissions) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)
	target := r.URL.Path

	user, err := models.GetUserByID(userId)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user %s", err)))
		return
	}

	request := SetPermissionsRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		w.WriteHeader(400)
		u.Respond(w, u.Message("error", "Invalid request"))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	for _, userId := range request.Read {
		currentUser, err := models.GetUserByID(userId)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user %s", err)))
			return
		}

		_, address, err := connection.GetKeyAndAddress(currentUser.EthereumPrivateKey)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to build address %s", err)))
			return
		}

		if err := connection.AddPermission(user.EthereumPrivateKey, target, *address, contract.ReadPermission); err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to set permissions for %s: %s", userId, err)))
			return
		}
	}

	for _, userId := range request.Write {
		currentUser, err := models.GetUserByID(userId)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user %s", err)))
			return
		}

		_, address, err := connection.GetKeyAndAddress(currentUser.EthereumPrivateKey)
		if err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to build address %s", err)))
			return
		}

		if err := connection.AddPermission(user.EthereumPrivateKey, target, *address, contract.WritePermission); err != nil {
			w.WriteHeader(500)
			u.Respond(w, u.Message("error", fmt.Sprintf("failed to set permissions for %s: %s", userId, err)))
			return
		}
	}
}

func GetSharedTree(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)

	user, err := models.GetUserByID(userId)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user %s", err)))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	result, err := connection.GetSharedDirectories(user.EthereumPrivateKey)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while getting shared dirs: %s", err)))
		return
	}

	resultJSON, err := json.Marshal(result)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while serializing shared dirs: %s", err)))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	if _, err := w.Write(resultJSON); err != nil {
		fmt.Println(err)
	}
}

type GetSharedFile struct{}

func (GetSharedFile) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fullPath := r.URL.Path
	fullPathParts := strings.Split(fullPath, "/")
	if len(fullPathParts) > 0 {
		if fullPathParts[0] == "" {
			fullPathParts = fullPathParts[1:]
		}
	}
	if len(fullPathParts) > 0 {
		if fullPathParts[len(fullPathParts)-1] == "" {
			fullPathParts = fullPathParts[:len(fullPathParts)-1]
		}
	}

	if len(fullPathParts) == 0 {
		w.WriteHeader(401)
		u.Respond(w, u.Message("error", "cannot get empty path"))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	user, err := models.GetUserByID(fullPathParts[0])
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user: %s", err)))
		return
	}

	targetPath := strings.Join(fullPathParts[1:], "/")
	permissions, err := connection.GetPermissions(user.EthereumPrivateKey, targetPath)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get permissions: %s", err)))
		return
	}

	currentUserId := r.Context().Value("user").(string)
	currentUser, err := models.GetUserByID(currentUserId)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user: %s", err)))
		return
	}

	_, address, err := connection.GetKeyAndAddress(currentUser.EthereumPrivateKey)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get address for the current user: %s", err)))
		return
	}

	isAllowedUser := false
	for _, allowedAddress := range append(permissions.Read, permissions.Write...) {
		isAllowedUser = isAllowedUser || (*address == allowedAddress)
	}

	if !isAllowedUser {
		w.WriteHeader(403)
		u.Respond(w, u.Message("error", "user not allowed"))
		return
	}

	file, err := service.DownloadFile(user.ID, targetPath)
	if err != nil {
		w.WriteHeader(400)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
		return
	}
	_, filename := filepath.Split(targetPath)
	w.Header().Set("Content-Disposition", "attachment; filename="+filename)
	w.Header().Set("Content-Type", mime.TypeByExtension(filepath.Ext(filename)))
	if _, err := w.Write(file.Data); err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
	}
}

type UploadToSharedDir struct{}

func (UploadToSharedDir) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fullPath := r.URL.Path
	fullPathParts := strings.Split(fullPath, "/")
	if len(fullPathParts) > 0 {
		if fullPathParts[0] == "" {
			fullPathParts = fullPathParts[1:]
		}
	}
	if len(fullPathParts) > 0 {
		if fullPathParts[len(fullPathParts)-1] == "" {
			fullPathParts = fullPathParts[:len(fullPathParts)-1]
		}
	}

	if len(fullPathParts) == 0 {
		w.WriteHeader(401)
		u.Respond(w, u.Message("error", "cannot get empty path"))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	user, err := models.GetUserByID(fullPathParts[0])
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user: %s", err)))
		return
	}

	targetPath := strings.Join(fullPathParts[1:], "/")
	permissions, err := connection.GetPermissions(user.EthereumPrivateKey, targetPath)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get permissions: %s", err)))
		return
	}

	currentUserId := r.Context().Value("user").(string)
	currentUser, err := models.GetUserByID(currentUserId)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get user: %s", err)))
		return
	}

	_, address, err := connection.GetKeyAndAddress(currentUser.EthereumPrivateKey)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to get address for the current user: %s", err)))
		return
	}

	isAllowedUser := false
	for _, allowedAddress := range permissions.Write {
		isAllowedUser = isAllowedUser || (*address == allowedAddress)
	}

	if !isAllowedUser {
		w.WriteHeader(403)
		u.Respond(w, u.Message("error", "user not allowed"))
		return
	}

	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer func() {
		file.Close()
		connection.Close()
	}()

	fileToSave := &models.File{}
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	fileToSave.Name = handler.Filename
	fileToSave.Path = filepath.Join(fullPathParts[0], targetPath)
	fileToSave.Data = fileBytes

	if err := service.AddNewFile(fullPathParts[0], fileToSave); err != nil {
		w.WriteHeader(404)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
		return
	}

	if err := connection.RegisterFilesystemNode(user.EthereumPrivateKey, targetPath, handler.Filename, true); err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to register dir: %s", err)))
		return
	}

	u.Respond(w, u.Message("message", "load file ok"))
}
