package models

type File struct {
	Data []byte
	Name string
	Path string
	CID  string
}
type Directory struct {
	Directory string `json:"directory"`
}
