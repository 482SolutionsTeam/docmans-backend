package main

import (
	"docmans/app"
	"docmans/controllers"
	"docmans/service"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	shell "github.com/ipfs/go-ipfs-api"
)

func main() {
	setup()
	r := mux.NewRouter()

	r.PathPrefix("/api").Methods("OPTIONS").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "authorization,content-type")
		w.Header().Set("Access-Control-Expose-Headers", "content-disposition")
	})
	r.Use(mux.CORSMethodMiddleware(r))

	r.Use(app.JwtAuthentication)

	r.HandleFunc("/api/register", controllers.CreateUser).Methods("POST")
	r.HandleFunc("/api/auth", controllers.Login).Methods("POST")
	r.HandleFunc("/api/logout", controllers.Logout).Methods("POST")
	r.HandleFunc("/api/directory", controllers.CreateDirectory).Methods("PUT")
	r.PathPrefix("/api/file/").Methods("PUT").Handler(http.StripPrefix("/api/file", controllers.UploadFile{}))
	r.PathPrefix("/api/file/").Methods("GET").Handler(http.StripPrefix("/api/file", controllers.DownloadFile{}))
	r.HandleFunc("/api/tree", controllers.GetTree).Methods("GET")
	r.PathPrefix("/api/permissions").Methods("GET").Handler(http.StripPrefix("/api/permissions", controllers.ReadPermissions{}))
	r.PathPrefix("/api/permissions").Methods("PUT").Handler(http.StripPrefix("/api/permissions", controllers.SetPermissions{}))
	r.HandleFunc("/api/shared/tree", controllers.GetSharedTree).Methods("GET")
	r.PathPrefix("/api/shared/file").Methods("PUT").Handler(http.StripPrefix("/api/shared/file", controllers.UploadToSharedDir{}))
	r.PathPrefix("/api/shared/file").Methods("GET").Handler(http.StripPrefix("/api/shared/file", controllers.GetSharedFile{}))

	log.Fatal(http.ListenAndServe(":3000", r))
}

func setup() {
	ipfsNodeUrl := "localhost:5001"             // load DOCMANS_IPFS_URL
	service.Shell = shell.NewShell(ipfsNodeUrl) // init shell resource in ipfs service
}
