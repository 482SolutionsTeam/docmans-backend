#!/bin/sh

_DOCKER_COMPOSE_ARGS=""
_BACKGROUND=0

while getopts "hd" arg; do
    case $arg in
        h)
            echo "Usage:\n\t-h\tShow this help.\n\t-d\tRun in background.\n"
            exit
            ;;
        d)
            _DOCKER_COMPOSE_ARGS="-d"
            _BACKGROUND=1
            ;;
    esac
done

# Check for the marker file created by the IPFS initialization script.
if [ ! -f "./storage/ipfs_initialized" ]; then
    echo "Initializing IPFS node..."
    docker-compose -f docker-compose-ipfs-setup.yml up
else
    echo "IPFS is already initialized!"
fi

echo "Starting the backend and the IPFS node..."

if [ $_BACKGROUND == 0 ]; then
    echo "Press Ctrl+C and run ./stop.sh to stop."
fi

docker-compose up $_DOCKER_COMPOSE_ARGS

if [ $_BACKGROUND == 1 ]; then
    echo "Run ./stop.sh to stop."
fi
