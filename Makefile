all: build

abigen:
	git submodule update --init
	abigen --pkg contract --sol docmans-smart-contracts/contracts/FileToken.sol --out contract/contract.go

build: abigen
	docker-compose build
