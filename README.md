# Docmans backend

This is the backend for Docmans - a distributed document management system.

## Prerequisites

- Docker
- Docker Compose 3.x
- `solc`: https://solidity.readthedocs.io/en/v0.4.24/installing-solidity.html
- `abigen` (just install Ethereum https://github.com/ethereum/go-ethereum)

## Build

To build the project run `make`.

## Run

To run the project enter `./start.sh`. If you want to run the project in the
background, enter `./start.sh -d`.

## Stopping the server

If you want to stop the server, run `./stop.sh`.
