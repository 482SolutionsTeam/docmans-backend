package utils

import (
	"crypto/sha1"
	"encoding/hex"
)

func Hash(data string) (hash string) {
	h := sha1.New()
	h.Write([]byte(data))
	hash = hex.EncodeToString(h.Sum(nil))
	return
}
