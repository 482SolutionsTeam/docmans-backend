package service

import (
	"docmans/models"
	"path/filepath"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/pkg/errors"
)

func DownloadFile(userId string, path string) (file *models.File, err error) {
	file = &models.File{}

	// step 2.1: resolve user's root dir cid based on IPNS
	log.Printf("Resolving users dir cid from %s ipns record", userId)
	peerId, err := findPeerId(userId)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to find peer id based on usedId:%s", userId)
	}

	resolvedCid, err := resolveIpns(peerId)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to resolve ipns from peerId:%s", peerId)
	}
	// step 2.2: get directory content
	// dst dir name is hash of user dir cid
	dst := userId
	log.Printf("Gettings whole user's dir into '%s' folder", dst)
	if err := getDir(resolvedCid, dst); err != nil {
		return nil, errors.Wrapf(err, "Failed to get user's %s dir from ipfs", userId)
	}

	fileStat, err := os.Stat(filepath.Join(dst, path))
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to read file, path: %s", dst+path)
	}

	// check that file is not dir
	if fileStat.IsDir() {
		errorMsg := fmt.Sprintf("Requested file is directory, path:%s", dst+path)
		return nil, errors.New(errorMsg)
	}

	// read file data
	data, err := ioutil.ReadFile(filepath.Join(dst, path))
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to read file data, path:%s", dst+path)
	}

	file.Name = fileStat.Name()
	file.Data = data

	// clean user dir from local storage
	clean(dst)

	return
}
