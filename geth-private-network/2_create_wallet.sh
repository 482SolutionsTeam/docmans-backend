#!/bin/bash

authorityAddr=$(curl -X POST -H "Content-Type: application/json" --data "{\"method\":\"personal_newAccount\",\"params\":[\"$1\"],\"id\":1,\"jsonrpc\":"2.0"}" localhost:8545 | jq -r ".result")
echo "Node address: $authorityAddr"
echo "Node password: $1"
docker-compose -f docker-compose.yaml -f _1_2_run_pow_network.yaml --project-name docmans_geth down
sed "s/1f24733db7e91dc733f3fe24ceafb815506b474a/${authorityAddr//0x}/g" genesis-clique.json.template > genesis-clique.json
docker-compose -f docker-compose.yaml -f _2_1_switch_to_clique.yaml --project-name docmans_geth up
docker-compose -f docker-compose.yaml -f _2_1_switch_to_clique.yaml --project-name docmans_geth down
