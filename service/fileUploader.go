package service

import (
	"docmans/models"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

// adds new file based on WGRND-92 scenario
func AddNewFile(userId string, file *models.File) error {
	// step 1: add file to IPFS
	// note: maybe load file to IPFS at the end, after all checks
	// what if we end up with error, and users directory defacto will stay
	// unchanged, but new file will be already published to IPFS as orphan
	if file == nil {
		return errors.New("file is nil")
	}

	log.Printf("Adding new file: name:%s, size:%dB", file.Name, len(file.Data))
	file.CID = addFile(file.Data)
	log.Printf("New file added, cid:%s", file.CID)

	// step 2: add file to user's dir
	// step 2.1: resolve user's root dir cid based on IPNS
	log.Printf("Resolving users dir cid from %s ipns record", userId)
	peerId, err := findPeerId(userId)
	if err != nil {
		return errors.Wrapf(err, "Failed to find peer id based on usedId:%s", userId)
	}

	resolvedCid, err := resolveIpns(peerId)
	if err != nil {
		return errors.Wrapf(err, "Failed to resolve ipns from peerId:%s", peerId)
	}
	// step 2.2: get directory content
	// dst dir name is hash of user dir cid
	dst := userId
	log.Printf("Gettings whole user's dir into '%s' folder", dst)
	if err := getDir(resolvedCid, dst); err != nil {
		return errors.Wrapf(err, "Failed to get user's %s dir from ipfs", userId)
	}

	// step 2.3: update dir with new file
	log.Print("Adding new file to dir")
	if err := updateDir(dst, file); err != nil {
		return errors.Wrap(err, "Failed to add new file")
	}

	// step 2.4: load updated dir to IPFS
	log.Print("Loading updated dir to IPFS")
	newDirCid, err := addDir(dst)
	if err != nil {
		return errors.Wrap(err, "Failed to load updated dir to ipfs")
	}
	log.Printf("New dir loaded, cid:%s", newDirCid)

	//step 2.5: assign dir new cid to user's ipns
	log.Print("Updating user's ipns record to point to update dir")
	log.Printf("'%s' --> '%s'", userId, newDirCid)
	if err := publish(newDirCid, userId); err != nil {
		return errors.Wrap(err, "Failed to upload new dir to ipfs")
	}

	// clean user dir from local storage
	clean(dst)

	return nil
}

func updateDir(dst string, file *models.File) error {
	dir, err := ioutil.ReadDir(file.Path)
	if err != nil {
		return errors.Wrap(err, "Failed to `ioutil.ReadDir`")
	}
	if dir == nil {
		return errors.New("Dir does not exist")
	}
	path := filepath.Join(file.Path, file.Name)
	if err := ioutil.WriteFile(path, file.Data, 0775); err != nil {
		return errors.Wrap(err, "Failed to write file")
	}
	return nil
}

func clean(path string) {
	os.RemoveAll(path)
}
