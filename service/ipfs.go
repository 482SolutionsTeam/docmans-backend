package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	shell "github.com/ipfs/go-ipfs-api"
	"github.com/pkg/errors"
)

var Shell *shell.Shell

const IPFS_API string = "http://127.0.0.1:5001"

type GetKeysResponse struct {
	Keys []struct {
		Name string
		Id   string
	}
}

func addFile(file []byte) (cid string) {
	cid, error := Shell.Add(bytes.NewReader(file))
	if error != nil {
		log.Fatalf("Failed to load file, error: %s", error)
	}
	return
}

// Function add selected dir recursively
// returm the root dir cid
func addDir(path string) (dirCid string, err error) {
	dirCid, err = Shell.AddDir(path)
	if err != nil {
		return "", errors.Wrap(err, "Failed to add dir, error: %s")
	}
	return
}

// todo how to operate with LsLink class ?
func listDir(dirCid string) (lsLinks []*shell.LsLink) {
	lsLinks, error := Shell.List(dirCid)
	if error != nil {
		log.Fatalf("Failed to get dir %s, error %s", dirCid, error)
	}
	return
}

func resolveIpns(peerId string) (cid string, err error) {
	resolveReq := fmt.Sprintf(IPFS_API+"/api/v0/name/resolve?arg=%s", peerId)
	resolveResp, err := http.Get(resolveReq)
	if err != nil {
		return "", errors.Wrapf(err, "Failed to resolve ipns name '%s'", peerId)
	}
	body, _ := ioutil.ReadAll(resolveResp.Body)
	msg := string(body)
	if resolveResp.StatusCode != 200 {
		errorMsg := fmt.Sprintf("Failed to resolve name:%s, http respose code:%d, api resp:%s", peerId, resolveResp.StatusCode, msg)
		return "", errors.New(errorMsg)
	}

	var res map[string]interface{}
	json.Unmarshal(body, &res)
	cid = fmt.Sprintf("%v", res["Path"])
	return
}

func genKey(keyName string) (peerId string, err error) {
	keygenReq := fmt.Sprintf(IPFS_API+"/api/v0/key/gen?arg=%s&type=rsa", keyName)
	keygenRes, err := http.Get(keygenReq)
	if err != nil {
		log.Fatalf("Failed to gen new key with name: %s", keyName)
	}

	body, _ := ioutil.ReadAll(keygenRes.Body)
	msg := string(body)

	if keygenRes.StatusCode != 200 {
		log.Fatalf("Failed to gen new key, http response code:%d, api resp:%s", keygenRes.StatusCode, msg)
	}

	var res map[string]interface{}
	json.Unmarshal(body, &res)
	peerId = fmt.Sprintf("%v", res["Id"])
	return
}

func publish(cid string, keyName string) error {
	publishReq := fmt.Sprintf(IPFS_API+"/api/v0/name/publish?arg=%s&key=%s&allow-offline=true", cid, keyName)
	publishRes, err := http.Get(publishReq)
	if err != nil {
		return errors.Wrap(err, "Failed to publish, error:%s")
	}
	body, _ := ioutil.ReadAll(publishRes.Body)
	msg := string(body)
	if publishRes.StatusCode != 200 {
		errMsg := fmt.Sprintf("Failed to publish, http respose code:%d, api resp:%s", publishRes.StatusCode, msg)
		return errors.New(errMsg)
	}
	return nil
}

// Gets the dir based on 'cid' and saves into 'dst' path locally
func getDir(cid string, dst string) error {
	if err := Shell.Get(cid, dst); err != nil {
		return errors.Wrapf(err, fmt.Sprintf("Failed to get dir:%s", cid))
	}
	return nil
}

func findPeerId(keyName string) (keyFound string, err error) {
	log.Printf("Looking for key with '%s' name", keyName)
	findPeerIdReq := fmt.Sprintf(IPFS_API + "/api/v0/key/list")
	findPeerIdRes, err := http.Get(findPeerIdReq)
	if err != nil {
		log.Fatalf("Failed to list keys")
	}

	body, _ := ioutil.ReadAll(findPeerIdRes.Body)
	msg := string(body)

	if findPeerIdRes.StatusCode != 200 {
		return "", errors.New(fmt.Sprintf("Failed to list keys, https response code:%d, message:%s", findPeerIdRes.StatusCode, msg))
	}

	keys := GetKeysResponse{}
	if err := json.Unmarshal(body, &keys); err != nil {
		return "", errors.Wrap(err, "invalid JSON")
	}

	for _, key := range keys.Keys {
		if key.Name == keyName {
			keyFound = key.Id
		}
	}

	if keyFound == "" {
		return "", errors.New("Could not find key with name")
	}

	return
}
