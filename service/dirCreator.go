package service

import (
	"log"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

// created new dir based on WGRND-103 scenario
func CreateDir(userId string, path string) (string, error) {

	// step 0: get locally user dir from ipfs based on ipns

	peerId, err := findPeerId(userId)
	if err != nil {
		return "", errors.Wrapf(err, "Failed to find peer id based on usedId:%s", userId)
	}

	resolvedCid, err := resolveIpns(peerId)
	if err != nil {
		return "", errors.Wrapf(err, "Failed to resolve ipns from peerId:%s", peerId)
	}

	dst := userId
	if err := getDir(resolvedCid, dst); err != nil {
		return "", errors.Wrapf(err, "Failed to get user's %s dir from ipfs", userId)
	}

	// step 1: create new dir locally
	log.Printf("Creating new dir, path:%s", dst)
	if _, err := os.Stat(dst); os.IsNotExist(err) {
		return "", errors.Wrap(err, "Target path does not exists to create new dir")
	}

	if _, err := os.Stat(filepath.Join(dst, path)); os.IsExist(err) {
		return filepath.Join(dst, path), errors.Wrap(err, "Directory already exists")
	}

	if err := os.Mkdir(filepath.Join(dst, path), os.ModePerm); err != nil {
		return "", errors.Wrapf(err, "Failed to create new dir, path:%s", filepath.Join(dst, path))
	}

	// step 2: upload updated user's root dir to ipfs
	log.Print("Loading updated dir to IPFS")
	newDirCid, err := addDir(dst)
	if err != nil {
		return "", errors.Wrap(err, "Failed to load updated dir to ipfs")
	}
	log.Printf("New dir loaded, cid:%s", newDirCid)

	// step 3:
	//step 2.5: assign dir new cid to user's ipns
	log.Print("Updating user's ipns record to point to update dir")
	log.Printf("'%s' --> '%s'", userId, newDirCid)
	if err := publish(newDirCid, userId); err != nil {
		return "", errors.Wrap(err, "Failed to upload new dir to ipfs")
	}

	// clean user dir from local storage
	clean(dst)
	return "", nil
}

func createDir(dst string, path string) error {
	if err := os.Mkdir(filepath.Join(dst, path), os.ModePerm); err != nil {
		return errors.Wrapf(err, "Failed to create new dir, path:%s", filepath.Join(dst, path))
	}

	return nil
}
