package models

import (
	"encoding/json"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"time"

	"fmt"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID                 string `json:"name"`
	PasswordHash       string `json:"password"`
	Token              string `json:"token"`
	EthereumPrivateKey string `json:"private_key"`
}

type Token struct {
	Token  string
	UserId string
	jwt.StandardClaims
}

func (user *User) Validate() error {
	if len(user.PasswordHash) < 6 {
		return errors.New("Password is required")
	}
	userByID, err := GetUserByID(user.ID)
	if err != nil {
		return errors.New(fmt.Sprintf("Connection error. Please retry %s", err))
	}
	if userByID != nil {
		return errors.New("User already exists")
	}
	return nil
}

func (user *User) Store() error {
	if err := user.Validate(); err != nil {
		return err
	}
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		return err
	}
	privateKeyBytes := crypto.FromECDSA(privateKey)
	user.EthereumPrivateKey = hexutil.Encode(privateKeyBytes)
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(user.PasswordHash), bcrypt.DefaultCost)
	user.PasswordHash = string(hashedPassword)
	userJson, err := json.Marshal(user)
	if err := ClientForUser.Set(user.ID, userJson, 0).Err(); err != nil {
		return err
	}
	return nil
}

func Login(id string, password string) (string, error) {
	user, err := GetUserByID(id)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("Connection error from models. Please retry %s", err))
	}
	if user == nil {
		return "", errors.New("No user")
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(password))
	if err != nil {
		return "", errors.New("Invalid Password")
	} else {
		tk := &Token{UserId: user.ID}
		tk.ExpiresAt = time.Now().Add((time.Hour * 24)).Unix()
		token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
		tokenString, _ := token.SignedString([]byte(os.Getenv("DOCMANS_JWT_SECRET")))
		user.Token = tokenString
		resp := user.Token
		return resp, nil
	}
}

func GetUserByID(id string) (*User, error) {
	userData, err := ClientForUser.Get(id).Result()
	fmt.Println(userData)
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "while getting user by ID")
	}
	user := &User{}
	if err := json.Unmarshal([]byte(userData), user); err != nil {
		return nil, errors.Wrap(err, "problems with unmarshal user")
	}
	return user, nil
}

func Logout(id string, token string) (bool, error) {

	byToken, err := GetToken(token)
	if byToken == nil {
		if err := ClientForToken.Set(token, id, time.Hour*24).Err(); err != nil {
			return false, err
		}
		return true, nil
	}
	return false, err
}

func GetToken(token string) (*Token, error) {
	name, err := ClientForToken.Get(token).Result()
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "while getting token")
	}
	tokenFromDB := &Token{}
	tokenFromDB.Token = token
	tokenFromDB.UserId = name
	return tokenFromDB, nil
}
