package controllers

import (
	"docmans/contract"
	"docmans/models"
	u "docmans/utils"
	"encoding/json"
	"fmt"
	ipfs "github.com/ipfs/go-ipfs-api"
	"io/ioutil"
	"net/http"
	"os"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		u.Respond(w, u.Message("error", "Invalid request"))
		return
	}
	errStore := user.Store()
	if errStore != nil {
		w.WriteHeader(409)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", errStore)))
		return
	}

	ipfsShell := ipfs.NewShell("localhost:5001")
	dirName, err := ioutil.TempDir("/ipfstmp", user.ID)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while creating tmp dir: %s", err)))
		return
	}

	cid, err := ipfsShell.AddDir(dirName)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while creating the root dir: %s", err)))
		return
	}

	keygenRequest := fmt.Sprintf("http://localhost:5001/api/v0/key/gen?arg=%s&type=rsa", user.ID)
	keygenResponse, err := http.Get(keygenRequest)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while generating a new ipns key: %s", err)))
		return
	}
	if keygenResponse.StatusCode != 200 {
		w.WriteHeader(500)
		body, _ := ioutil.ReadAll(keygenResponse.Body)
		u.Respond(w, u.Message("error", fmt.Sprintf("while generating a new ipns key: %s", string(body))))
		return
	}

	ipnsRequest := fmt.Sprintf("http://localhost:5001/api/v0/name/publish?arg=%s&key=%s&allow-offline=true", cid, user.ID)
	ipnsResponse, err := http.Get(ipnsRequest)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while binding ipns name: %s", err)))
		return
	}
	if ipnsResponse.StatusCode != 200 {
		w.WriteHeader(500)
		body, _ := ioutil.ReadAll(keygenResponse.Body)
		u.Respond(w, u.Message("error", fmt.Sprintf("while generating a new ipns key: %s", string(body))))
		return
	}

	connection, err := contract.MakeConnection(
		os.Getenv("DOCMANS_ETH_NODE_URL"),
		os.Getenv("DOCMANS_ETH_SMART_CONTRACT_ADDR"),
	)
	if err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("while connecting to Eth node: %s", err)))
		return
	}
	defer connection.Close()

	if err := connection.RegisterRootDir(user.EthereumPrivateKey, user.ID); err != nil {
		w.WriteHeader(500)
		u.Respond(w, u.Message("error", fmt.Sprintf("failed to register root dir: %s", err)))
		return
	}

	u.Respond(w, u.Message("message", "registration ok"))
}

func Login(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		u.Respond(w, u.Message("error", "Invalid request"))
		return
	}
	resp, err := models.Login(user.ID, user.PasswordHash)
	if err != nil {
		w.WriteHeader(409)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
		return
	}
	u.Respond(w, u.Message("token", resp))
}

func Logout(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("user").(string)
	token := r.Context().Value("token").(string)
	result, err := models.Logout(userId, token)
	if err != nil {
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
	}

	if result == true {
		r.Header.Del("Authorization")
		u.Respond(w, u.Message("message", "Logout ok"))
	}
	if result == false {
		u.Respond(w, u.Message("error", "Bad Authorization"))
	}
}
