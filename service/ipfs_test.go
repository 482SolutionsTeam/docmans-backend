package service

import (
	"log"
	"os"
	"testing"

	shell "github.com/ipfs/go-ipfs-api"
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func setup() {
	ipfsNodeUrl := "127.0.0.1:5001"
	Shell = shell.NewShell(ipfsNodeUrl)
}

func TestFindPeerId(t *testing.T) {
	name := "userID"

	expected := "QmUpz4K9127G1afcHtxRihm9fVc4S1bcV6U8eNvqFZbwnf"
	actual, err := findPeerId(name)
	if err != nil {
		log.Fatalf("Failed to find peer id, error:%s", err)
	}

	if actual != expected {
		log.Fatalf("Result does not match;\nexpected:%s\nactual:%s", expected, actual)
	}
}

func TestIpfsPublish(t *testing.T) {
	testPath := "./../test/resources/ipfsLoadData/"
	userId := "userID"

	peerId, err := findPeerId(userId)
	if err != nil {
		log.Fatalf("Failed to find peer id")
	}

	cid, err := addDir(testPath + "IN")
	if err != nil {
		log.Fatalf("Failed to load dir to ipfs")
	}
	log.Printf("Added new dir: %s", cid)

	publish(cid, userId)

	resolvedCid, err := resolveIpns(peerId)
	if err != nil {
		log.Fatalf("Failed to resolve ipns name, error:%s", err)
	}
	getDir(resolvedCid, testPath+"OUT")

}
