#!/bin/sh

echo "Stopping the IPFS node and the backend instance..."
docker-compose down
echo "Done!"
