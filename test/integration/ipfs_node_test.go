package integration

import (
	"bytes"
	"log"
	"testing"

	shell "github.com/ipfs/go-ipfs-api"
)

func TestIpfsNodeConnection(t *testing.T) {
	sh := shell.NewShell("localhost:5001")

	// try to add file
	file := []byte("my test file is a dog gif")
	cid, error := sh.Add(bytes.NewReader(file))
	if error != nil {
		t.Fail()
		log.Fatalf("Failed to load file, error: %s", error)
	}
	log.Printf("File succesufully, cid:%s", cid)
}
