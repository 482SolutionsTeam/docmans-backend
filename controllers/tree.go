package controllers

import (
	"docmans/service"
	u "docmans/utils"
	"fmt"
	"net/http"
)

func GetTree(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(string)

	result, err := service.Tree(user)
	if err != nil {
		w.WriteHeader(404)
		u.Respond(w, u.Message("error", fmt.Sprintf("%s", err)))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	if _, err := w.Write(result); err != nil {
		fmt.Println(err)
	}
}
